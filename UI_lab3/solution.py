import csv
import math
import sys


def decisionFunction(decisionTree, test, znacajke, data):
    for key in decisionTree.keys():
        for way in decisionTree[key]:
            if way[0] == test[znacajke.index(key)]:
                if type(way[1]) != dict:
                    return way[1]
                else:
                    dataCopy = data.copy()
                    D = []
                    for row in dataCopy:
                        if row[znacajke.index(key)] == test[znacajke.index(key)]:
                            D.append(row)
                    return decisionFunction(way[1], test, znacajke, D)
    else:
        nums = {}
        maxn = 0
        v = ""
        for row in data:
            if row[-1] in nums.keys():
                nums[row[-1]] += 1
            else:
                nums[row[-1]] = 1
        for key in nums.keys():
            if nums[key] > maxn:
                maxn = nums[key]
                v = key
            elif nums[key] == maxn:
                if key < v:
                    maxn = nums[key]
                    v = key
        return v


def printFunction(decisionTree, depth, actualDepth, oznake_klase):
    toPrint = []
    if type(decisionTree) != list and depth != 0 and decisionTree[1] in oznake_klase:
        return ""
    if depth % 2 == 0:
        if depth == 0:
            for key in decisionTree.keys():
                printStr = str(actualDepth) + ":" + key
                toPrint.append(printStr)
                if len(decisionTree[key]) != 0:
                    toPrint += printFunction(decisionTree[key], depth + 1, actualDepth + 1, oznake_klase)
        else:
            for key in decisionTree[1].keys():
                printStr = str(actualDepth) + ":" + key
                toPrint.append(printStr)
                if len(decisionTree[1][key]) != 0:
                    toPrint += printFunction(decisionTree[1][key], depth + 1, actualDepth + 1, oznake_klase)
    else:
        for i in range(len(decisionTree)):
            toPrint += printFunction(decisionTree[i], depth + 1, actualDepth, oznake_klase)
    return toPrint


def calcED(oznake_klase, D):
    ED = 0
    nums = {row: 0 for row in oznake_klase}
    for row in D:
        nums[row[-1]] += 1
    for key in nums.keys():
        ED -= (nums[key] / len(D)) * math.log2(nums[key] / len(D))
    return ED


def IG(D, x, oznake_klase, ED):
    variables = {}
    returnList = []
    for row in D:
        if row[x] in variables:
            variables[row[x]] += 1
        else:
            variables[row[x]] = 1
    for key in variables.keys():
        values = {row: 0 for row in oznake_klase}
        value = 0
        sumElems = 0
        for row in D:
            if row[x] == key:
                values[row[-1]] += 1
                sumElems += 1
        if 0 in values.values():
            value = 0
        else:
            for key2 in values.keys():
                value -= (values[key2] / sumElems) * math.log2(values[key2] / sumElems)
        ED -= (variables[key] / len(D)) * value
        returnList.append([key, value])
    return ED, returnList


def id3(train1, train2, znacajke, oznake_klase, depth):
    v = ""
    if len(train1) == 0:
        maxn = 0
        nums = {row: 0 for row in oznake_klase}
        for row in train2:
            nums[row[-1]] += 1
        for key in nums.keys():
            if nums[key] > maxn:
                maxn = nums[key]
                v = key
        return v
    maxn = 0
    nums = {row: 0 for row in oznake_klase}
    for row in train1:
        nums[row[-1]] += 1
    for key in nums.keys():
        if nums[key] > maxn:
            maxn = nums[key]
            v = key
        elif nums[key] == maxn:
            if key < v:
                maxn = nums[key]
                v = key
    for row in train1:
        if row[-1] != v:
            break
    else:
        return v
    if len(znacajke) == 0:
        return v
    x = 0
    biggest = 0
    iterationList = []
    ED = calcED(oznake_klase, train1)
    for i in range(len(znacajke)):
        new, tempList = IG(train1, i, oznake_klase, ED)
        if new > x:
            x = new
            biggest = i
            iterationList = tempList
        elif new == x:
            if znacajke[i] <= znacajke[biggest]:
                x = new
                biggest = i
                iterationList = tempList
    znacajkeCopy = znacajke.copy()
    znacajkeCopy.remove(znacajke[biggest])
    subtrees = []
    if depth == 1:
        options = {}
        for elem in iterationList:
            for row in train1:
                if elem[0] == row[biggest]:
                    for oznaka in oznake_klase:
                        if oznaka == row[-1]:
                            if oznaka in options:
                                options[oznaka] += 1
                            else:
                                options[oznaka] = 1
            maxn = 0
            v = ""
            for key in options.keys():
                if options[key] > maxn:
                    maxn = options[key]
                    v = key
                elif options[key] == maxn:
                    if key < v:
                        maxn = options[key]
                        v = key
            subtrees.append((elem[0], v))
            options.clear()
        subtreesReturn = {znacajke[biggest]: subtrees}
        return subtreesReturn
    for elem in iterationList:
        D = []
        for row in train1:
            if row[biggest] == elem[0]:
                rowCopy = row.copy()
                rowCopy.pop(biggest)
                D.append(rowCopy)
        t = id3(D, train1, znacajkeCopy, oznake_klase, depth - 1)
        subtrees.append((elem[0], t))
    subtreesReturn = {znacajke[biggest]: subtrees}
    return subtreesReturn


class ID3:
    mode = "test"
    model = "ID3"
    max_depth = float("inf")
    num_trees = 1
    feature_ratio = 1
    example_ratio = 1
    decisionTree = {}
    znacajke = []
    oznake_klase = []
    train = []

    def __init__(self, config):
        for row in config:
            if row.split("=")[0] == "mode":
                self.mode = row.split("=")[1]
            elif row.split("=")[0] == "model":
                self.model = row.split("=")[1]
            elif row.split("=")[0] == "max_depth":
                if int(row.split("=")[1]) != -1:
                    self.max_depth = int(row.split("=")[1])
            elif row.split("=")[0] == "num_trees":
                self.num_trees = int(row.split("=")[1])
            elif row.split("=")[0] == "feature_ratio":
                self.feature_ratio = float(row.split("=")[1])
            elif row.split("=")[0] == "example_ratio":
                self.example_ratio = float(row.split("=")[1])

    def fit(self, train):
        znacajke = train.pop(0)
        self.train = train
        znacajke.pop()
        self.znacajke = znacajke
        self.oznake_klase = list(set([row[-1] for row in train]))
        self.decisionTree = id3(train, train, znacajke, self.oznake_klase, self.max_depth)
        toPrint = printFunction(self.decisionTree, 0, 0, self.oznake_klase)
        for i in range(len(toPrint)):
            if i != len(toPrint) - 1:
                print(toPrint[i], end=", ")
            else:
                print(toPrint[i])

    def predict(self, test):
        test.pop(0)
        predictions = []
        errorMatrix = {}
        cnt = 0
        correct = 0
        for row in test:
            predictions.append(decisionFunction(self.decisionTree, row, self.znacajke, self.train))
        for i in predictions:
            print(i, end=" ")
            if predictions[cnt] == test[cnt][-1]:
                correct += 1
            cnt += 1
        print("\n%.5f" % (correct / len(predictions)))
        for i in range(len(predictions)):
            if (predictions[i], test[i][-1]) in errorMatrix:
                errorMatrix[(predictions[i], test[i][-1])] += 1
            else:
                errorMatrix[(predictions[i], test[i][-1])] = 1
        for i in self.oznake_klase:
            for j in self.oznake_klase:
                if (i, j) not in errorMatrix.keys():
                    errorMatrix[i, j] = 0
        self.oznake_klase.sort()
        printList = []
        for i in self.oznake_klase:
            for key in errorMatrix.keys():
                if key[1] == i:
                    printList.append(key)
            else:
                printList = sorted(printList, key=lambda x: x[0])
                for l in printList:
                    print(errorMatrix[l], end=" ")
                printList.clear()
                print("")


train = []
test = []

with open(sys.argv[1], encoding="utf-8") as csvfile:
    trainData = csv.reader(csvfile)
    for row in trainData:
        train.append(row)

with open(sys.argv[2], encoding="utf-8") as csvfile:
    testData = csv.reader(csvfile)
    for row in testData:
        test.append(row)

with open(sys.argv[3], encoding="utf-8") as cfgfile:
    config = cfgfile.readlines()

config = [x.strip() for x in config]

model = ID3(config)
model.fit(train)
model.predict(test)
