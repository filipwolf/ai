import heapq

def backtrace(parent, S, goal):
    p = parent[goal]
    trace_list = []
    trace_list.append(goal)
    while p is not S:
        trace_list.append(p)
        p = parent[p]
    trace_list.append(S)
    return trace_list

def UCS(S, state_space_dict, goal):
    parent = {}
    open = []
    visited = set()
    heapq.heapify(open)
    heapq.heappush(open, S)
    c = 0
    while len(open) is not 0:
        if len(visited) is not 0:
            n = heapq.heappop(open)
            c = n[0]
            n = n[1]
        else:
            n = heapq.heappop(open)
        if n in goal:
            print("States visited: " + str(len(visited) + 1))
            return backtrace(parent, S, n)
        visited.add(n)
        flag = False
        for m in state_space_dict[n]:
            m = m.rstrip()
            split2 = m.split(",")
            split2[1] = int(split2[1]) + c
            if "123_456_78x" not in goal:
                for i in open:
                    if i[1] == split2[0] and split2[1] > i[0]:
                        flag = True
                        break
                if flag is True:
                    continue
            if split2[0] not in visited:
                split2.reverse()
                heapq.heappush(open, split2)
                #open.sort(key=lambda x: x[1])
                parent[split2[1]] = n
                #print(len(visited))



cnt = 0
state_space = []
state_space_dict = {}

with open("3x3_puzzle.txt") as f:
    state_space = f.readlines()

if "#\n" in state_space:
    state_space.remove("#\n")

S = state_space.pop(0).rstrip()
goal1 = state_space.pop(0).split(" ")

goal = []
for i in goal1:
    goal.append(i.rstrip())

for l in state_space:
    split1 = l.split(":")
    split2 = split1[1].split(" ")
    state_space_dict[split1[0]] = split2

cnt = 0

for i in state_space_dict.values():
    if '' in i:
        i.remove('')
    if '\n' in i:
        i.remove('\n')
for i in state_space_dict:
    for j in state_space_dict[i]:
        cnt += 1

print("Start state: " + S)
print("End state(s): " + str(goal))
print("State space size: " + str(len(state_space_dict)))
print("Total transitions: " + str(cnt))

#print(state_space_dict)

final = UCS(S,state_space_dict, goal)

final.reverse()

cnt = 0

for i in range(0, len(final)):
    #print(state_space_dict[final[i]])
    for y in state_space_dict[final[i]]:
        list3 = y.split(",")
        if i <= len(final) - 2 and list3[0] == final[i+1]:
            cnt+=int(list3[1].rstrip())

print("Found path of length " + str(len(final)) + " with total cost " + str(cnt) + ":")

for state in final:
    if state not in  goal:
        print(state + " =>")
    else:
        print(state)