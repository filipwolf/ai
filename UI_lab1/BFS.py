def backtrace(parent, S, goal):
    p = parent[goal]
    trace_list = []
    trace_list.append(goal)
    while p is not S:
        trace_list.append(p)
        p = parent[p]
    trace_list.append(S)
    return trace_list

def BFS(S, state_space_dict, goal):
    parent = {}
    open = []
    visited = set()
    open.append(S)
    while len(open) is not 0:
        if len(visited) is not 0:
            split1 = open.pop(0).split(",")
            n = split1[0]
        else:
            n = open.pop(0).rstrip()
        if n in goal:
            print("States visited: " + str(len(visited) + 1))
            return backtrace(parent, S, n)
        visited.add(n)
        for m in state_space_dict[n]:
            split2 = m.split(",")
            if split2[0] not in visited:
                open.append(m.rstrip())
                parent[split2[0]] = n
                #print(len(visited))



cnt = 0
state_space = []
state_space_dict = {}

with open("3x3_puzzle.txt") as f:
    state_space = f.readlines()

if "#\n" in state_space:
    state_space.remove("#\n")

S = state_space.pop(0).rstrip()
goal1 = state_space.pop(0).split(" ")

goal = []
for i in goal1:
    goal.append(i.rstrip())

for l in state_space:
    split1 = l.split(":")
    split2 = split1[1].split(" ")
    state_space_dict[split1[0]] = split2

cnt = 0

for i in state_space_dict.values():
    if '' in i:
        i.remove('')
    if '\n' in i:
        i.remove('\n')
for i in state_space_dict:
    for j in state_space_dict[i]:
        cnt += 1

print("Start state: " + S)
print("End state(s): " + str(goal))
print("State space size: " + str(len(state_space_dict)))
print("Total transitions: " + str(cnt))

#print(state_space_dict)

final = BFS(S,state_space_dict, goal)

final.reverse()

cnt = 0

for i in range(0, len(final)):
    #print(state_space_dict[final[i]])
    for y in state_space_dict[final[i]]:
        list3 = y.split(",")
        if i <= len(final) - 2 and list3[0] == final[i+1]:
            cnt+=int(list3[1].rstrip())

print("Found path of length " + str(len(final)) +":")

for state in final:
    if state not in  goal:
        print(state + " =>")
    else:
        print(state)