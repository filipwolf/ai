import heapq

def backtrace(parent, S, goal):
    p = parent[goal]
    trace_list = []
    trace_list.append(goal)
    while p is not S:
        trace_list.append(p)
        p = parent[p]
    trace_list.append(S)
    return trace_list

def AStar(S, state_space_dict, goal, heuristics):
    parent = {}
    open = []
    closed = []
    visited = set()
    heapq.heapify(open)
    heapq.heappush(open, S)
    n1 = []
    c = 0
    while len(open) is not 0:
        if len(visited) is not 0:
            n1 = heapq.heappop(open)
            c = n1[1]
            n = n1[2]
        else:
            n = heapq.heappop(open)
            n1.append(n)
            n1.append(0)
            n1.append(0)
        if n in goal:
            print("States visited: " + str(len(set(visited)) + 1))
            return backtrace(parent, S, n)
        visited.add(n)
        closed.append(n1)
        for m in state_space_dict[n]:
            flag = False
            m = m.rstrip()
            split2 = m.split(",")
            split2[1] = int(split2[1]) + c
            predict = split2[1]
            if split2[0] in heuristics:
                predict += heuristics[split2[0]]
            if "123_456_78x" not in goal:
                for i in open:
                    if i[2] == split2[0] and split2[1] >= i[1]:
                        flag = True
                    if i[2] == split2[0] and split2[1] < i[1]:
                        open.remove(i)
                if flag is True:
                    continue
                for i in closed:
                    if i[2] == split2[0] and split2[1] >= i[1]:
                        flag = True
                    if i[2] == split2[0] and split2[1] < i[1]:
                        open.remove(i)
                if flag is True:
                    continue
            if split2[0] not in visited:
                split2.append(predict)
                split2.reverse()
                heapq.heappush(open, split2)
                #open.sort(key=lambda x: x[2])
                parent[split2[2]] = n
                #print(len(visited))



cnt = 0
state_space = []
state_space_dict = {}
heuristics = []
heuristics_dict = {}

with open("ai.txt", encoding="utf8") as f:
    state_space = f.readlines()

with open("ai_fail.txt", encoding="utf8") as f:
    heuristics = f.readlines()

for i in heuristics:
    split1 = i.split(":")
    heuristics_dict[split1[0]] = int(split1[1])

if "#\n" in state_space:
    state_space.remove("#\n")

S = state_space.pop(0).rstrip()
goal1 = state_space.pop(0).split(" ")

goal = []
for i in goal1:
    goal.append(i.rstrip())

for l in state_space:
    split1 = l.split(":")
    split2 = split1[1].split(" ")
    state_space_dict[split1[0]] = split2

cnt = 0

for i in state_space_dict.values():
    if '' in i:
        i.remove('')
    if '\n' in i:
        i.remove('\n')
for i in state_space_dict:
    for j in state_space_dict[i]:
        cnt += 1

print("Start state: " + S)
print("End state(s): " + str(goal))
print("State space size: " + str(len(state_space_dict)))
print("Total transitions: " + str(cnt))

#print(state_space_dict)

final = AStar(S,state_space_dict, goal, heuristics_dict)

final.reverse()

cnt = 0

for i in range(0, len(final)):
    #print(state_space_dict[final[i]])
    for y in state_space_dict[final[i]]:
        list3 = y.split(",")
        if i <= len(final) - 2 and list3[0] == final[i+1]:
            cnt+=int(list3[1].rstrip())

print("Found path of length " + str(len(final)) + " with total cost " + str(cnt) + ":")

for state in final:
    if state not in  goal:
        print(state + " =>")
    else:
        print(state)