cnt = 0
state_space = []
state_space_dict = {}
heuristics = []

with open("istra.txt") as f:
    state_space = f.readlines()

with open("istra_pessimistic_heuristic.txt") as f:
    heuristics = f.readlines()

if "#\n" in state_space:
    state_space.remove("#\n")

S = state_space.pop(0).rstrip()
goal1 = state_space.pop(0).split(" ")

goal = []
for i in goal1:
    goal.append(i.rstrip())

for l in state_space:
    split1 = l.split(":")
    split2 = split1[1].split(" ")
    state_space_dict[split1[0]] = split2

for i in state_space_dict.values():
    if '' in i:
        i.remove('')

#print(state_space_dict)

n = []
n.append(goal[0])
n.append(0)
n.append(0)
open = []
checked = []
flag = True
flag2 = False

state_space_dict2 = {}

if "enroll_artificial_intelligence" in state_space_dict:
    for a in state_space_dict:
        list1 = []
        for b in state_space_dict:
            for c in state_space_dict[b]:
                split = c.split(",")
                if split[0] == a:
                    list1.append(b + "," + split[1])
        state_space_dict2[a] = list1
    state_space_dict.update(state_space_dict2)

for l in goal:
    n = []
    n.append(l)
    n.append(0)
    n.append(0)
    flag = True
    state_space_dict.update(state_space_dict2)
    checked.clear()
    while len(state_space_dict) != 0:
        if len(open) == 0 and flag is False:
            break
        if n[0] not in state_space_dict.keys() and flag is False:
            n = open.pop(0)
            #print(n)
            continue
        for state in state_space_dict[n[0]]:
            split2 = state.split(",")
            for i in heuristics:
                split1 = i.split(":")
                if split1[0] == split2[0]:
                    for j in heuristics:
                        split3 = j.split(":")
                        if split3[0] == n[0]:
                            if int(split1[1]) > int(split2[1]) + int(split3[1]):
                                if split2[0] not in checked:
                                    print("h(" + split2[0] + ") > h(" + n[0] + ") + c: " + split1[1].rstrip() + " > " + str(int(split3[1])) + " + " + str(int(split2[1])))
                                    flag2 = True
                                    checked.append(split2[0])
            split2.append(int(split2[1]) + n[2])
            open.append(split2)
        del state_space_dict[n[0]]
        n = open.pop(0)
        c = n[2]
        #print(n)
        flag = False

if flag2 is True:
    print("Heuristic is not consistent")
else:
    print("Heuristic is consistent")