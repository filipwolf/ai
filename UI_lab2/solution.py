import sys


def brisanje(clauses):
    flag = False
    while flag is False:
        flag2 = True
        for i in clauses:
            for j in clauses:
                if not i == j:
                    if set(tuple(x) for x in i).issubset(tuple(x) for x in j):
                        if len(i) > len(j):
                            clauses.remove(i)
                            flag2 = False
                        elif len(i) < len(j):
                            clauses.remove(j)
                            flag2 = False
        if flag2 is True:
            flag = True

    flag = False
    for i in clauses:
        for j in i:
            for k in i:
                if k == "~" + j or j == "~" + k:
                    clauses.remove(i)
                    flag = True
                    break
            if flag:
                break


def plResolve(clauses, SoS, new, parents, indices):
    for i in clauses:
        for j in SoS:
            for k in i:
                for l in j:
                    if len(i) == 1 and len(j) == 1 and (k == "~" + l or l == "~" + k):
                        parents.append(indices[repr(i)])
                        parents.append(indices[repr(j)])
                        return ["NIL"]
                    if k == "~" + l or l == "~" + k:
                        i1 = i.copy()
                        j1 = j.copy()
                        i1.remove(k)
                        j1.remove(l)
                        if i1 + j1 in new:
                            continue
                        parents.append(indices[repr(i)])
                        parents.append(indices[repr(j)])
                        return i1 + j1
    for i in SoS:
        for j in SoS:
            for k in i:
                for l in j:
                    if len(i) == 1 and len(j) == 1 and (k == "~" + l or l == "~" + k):
                        parents.append(indices[repr(i)])
                        parents.append(indices[repr(j)])
                        return ["NIL"]
                    if k == "~" + l or l == "~" + k:
                        i1 = i.copy()
                        j1 = j.copy()
                        i1.remove(k)
                        j1.remove(l)
                        if i1 + j1 in new:
                            continue
                        parents.append(indices[repr(i)])
                        parents.append(indices[repr(j)])
                        return i1 + j1

    return []


def plResolution(clauses, cnt, indices, last, numOfClauses, verbose):
    new = []
    if numOfClauses == 1:
        SoS = [clauses[len(clauses) - 1]]
    else:
        SoS = []
        for i in range(numOfClauses):
            SoS.append(clauses[len(clauses) - i - 1])
    parents = []

    while True:
        newClause = plResolve(clauses, SoS, new, parents, indices)
        if len(newClause) == 0:
            print(last + " is unknown")
            return last + " is unknown"
        if verbose == "verbose":
            if not len(newClause) == 1:
                string = ""
                for j in range(0, len(newClause)):
                    if not j == len(newClause) - 1:
                        string += newClause[j] + " v "
                    else:
                        string += newClause[j]
                print(str(cnt) + ". " + string + " (" + str(parents[0]) + ", " + str(parents[1]) + ")")
            else:
                print(str(cnt) + ". " + newClause[0] + " (" + str(parents[0]) + ", " + str(parents[1]) + ")")
        indices[repr(newClause)] = cnt
        if "NIL" in newClause:
            print(last + " is true")
            return last + " is true"
        cnt += 1
        parents.clear()
        SoS = [x for x in SoS if x != []]
        clauses = [x for x in clauses if x != []]
        new.append(newClause.copy())
        SoS.insert(0, newClause)
        brisanje(SoS)


if sys.argv[1] == "resolution":

    if len(sys.argv) >= 4:
        verbose = sys.argv[len(sys.argv) - 1]
    else:
        verbose = ""

    with open(sys.argv[2]) as f:
        clauses = f.readlines()

    for i in clauses:
        if i.startswith("#"):
            clauses.remove(i)

    clauses = list(map(str.strip, clauses))
    clauses = list(map(str.lower, clauses))

    numOfClauses = 0
    last = clauses[len(clauses) - 1]

    if " v " in clauses[len(clauses) - 1]:
        split1 = clauses[len(clauses) - 1].split(" v ")
        split1 = list(map(str.strip, split1))
        numOfClauses = len(split1)
        clauses.remove(clauses[len(clauses) - 1])
        for i in split1:
            if "~" in i:
                clauses.append(i[1:])
            else:
                clauses.append("~" + i)
    else:
        numOfClauses = 1
        if "~" in clauses[len(clauses) - 1]:
            clauses[len(clauses) - 1] = clauses[len(clauses) - 1][1:]
        else:
            clauses[len(clauses) - 1] = "~" + clauses[len(clauses) - 1]

    for i in range(len(clauses)):
        if " " in clauses[i]:
            split1 = clauses[i].split(" v ")
            split1 = list(map(str.strip, split1))
            clauses[i] = split1
        else:
            clauses[i] = [clauses[i]]

    brisanje(clauses)
    cnt = 1

    indices = {}

    for i in clauses:
        if verbose == "verbose":
            if not len(i) == 1:
                string = ""
                for j in range(0, len(i)):
                    if not j == len(i) - 1:
                        string += i[j] + " v "
                    else:
                        string += i[j]
                print(str(cnt) + ". " + string)
            else:
                print(str(cnt) + ". " + i[0])
        indices[repr(i)] = cnt
        cnt += 1

    plResolution(clauses, cnt, indices, last, numOfClauses, verbose)

elif sys.argv[1] == "cooking_test":

    if len(sys.argv) >= 5:
        verbose = sys.argv[len(sys.argv) - 1]
    else:
        verbose = ""

    with open(sys.argv[2]) as f:
        clauses = f.readlines()
    with open(sys.argv[3]) as f:
        queries = f.readlines()

    for i in clauses:
        if i.startswith("#"):
            clauses.remove(i)

    clauses = list(map(str.strip, clauses))
    clauses = list(map(str.lower, clauses))
    queries = list(map(str.strip, queries))
    queries = list(map(str.lower, queries))

    for i in range(len(clauses)):
        if " " in clauses[i]:
            split1 = clauses[i].split(" v ")
            split1 = list(map(str.strip, split1))
            clauses[i] = split1
        else:
            clauses[i] = [clauses[i]]

    for i in queries:
        split1 = i.split(" ")
        if split1[1] == "+":
            clauses.append([split1[0]])
            brisanje(clauses)
        if split1[1] == "-" and [split1[0]] in clauses:
            clauses.remove([split1[0]])
        if split1[1] == "?":
            last = split1[0]
            if " v " in split1[0]:
                split2 = split1[0].split(" v ")
                split2 = list(map(str.strip, split2))
                numOfClauses = len(split2)
                for j in split2:
                    if "~" in j:
                        clauses.append([j[1:]])
                    else:
                        clauses.append(["~" + j])
            else:
                numOfClauses = 1
                if "~" in split1[0]:
                    clauses.append([split1[0][1:]])
                else:
                    clauses.append(["~" + split1[0]])

            clausesChange = clauses.copy()
            brisanje(clauses)
            cnt = 1

            indices = {}

            for j in clauses:
                if verbose == "verbose":
                    if not len(j) == 1:
                        string = ""
                        for k in range(0, len(j)):
                            if not k == len(j) - 1:
                                string += j[k] + " v "
                            else:
                                string += j[k]
                        print(str(cnt) + ". " + string)
                    else:
                        print(str(cnt) + ". " + j[0])
                indices[repr(j)] = cnt
                cnt += 1
            result = plResolution(clausesChange, cnt, indices, last, numOfClauses, verbose)
            clausesChange.pop()
            clauses = clausesChange.copy()

elif sys.argv[1] == "cooking_interactive":

    if len(sys.argv) >= 4:
        verbose = sys.argv[len(sys.argv) - 1]
    else:
        verbose = ""

    with open(sys.argv[2]) as f:
        clauses = f.readlines()

    for i in clauses:
        if i.startswith("#"):
            clauses.remove(i)

    clauses = list(map(str.strip, clauses))
    clauses = list(map(str.lower, clauses))

    for i in range(len(clauses)):
        if " " in clauses[i]:
            split1 = clauses[i].split(" v ")
            split1 = list(map(str.strip, split1))
            clauses[i] = split1
        else:
            clauses[i] = [clauses[i]]

    txt = ""

    while True:

        txt = input()

        if txt == "exit":
            exit()

        split1 = txt.split(" ")
        split1 = list(map(str.lower, split1))
        if split1[1] == "+":
            clauses.append([split1[0]])
            brisanje(clauses)
        if split1[1] == "-" and [split1[0]] in clauses:
            clauses.remove([split1[0]])
        if split1[1] == "?":
            last = split1[0]
            if " v " in split1[0]:
                split2 = split1[0].split(" v ")
                split2 = list(map(str.strip, split2))
                numOfClauses = len(split2)
                for j in split2:
                    if "~" in j:
                        clauses.append([j[1:]])
                    else:
                        clauses.append(["~" + j])
            else:
                numOfClauses = 1
                if "~" in split1[0]:
                    clauses.append([split1[0][1:]])
                else:
                    clauses.append(["~" + split1[0]])

            clausesChange = clauses.copy()
            brisanje(clauses)
            cnt = 1

            indices = {}

            for j in clauses:
                if verbose == "verbose":
                    if not len(j) == 1:
                        string = ""
                        for k in range(0, len(j)):
                            if not k == len(j) - 1:
                                string += j[k] + " v "
                            else:
                                string += j[k]
                        print(str(cnt) + ". " + string)
                    else:
                        print(str(cnt) + ". " + j[0])
                indices[repr(j)] = cnt
                cnt += 1
            result = plResolution(clausesChange, cnt, indices, last, numOfClauses, verbose)
            clausesChange.pop()
            clauses = clausesChange.copy()
